﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Windows.Forms;
using Med_X.Fenetres;

namespace Med_X
{
    public partial class fen_accueil : Form
    {
        public fen_accueil()
        {
            InitializeComponent();
            
        }

        private void Fen_accueil_Load(object sender, EventArgs e)
        {            
         
            fonctionAffichePatient("");

            /*txt_filtreLit.Select();//
            txt_filtreLit.LostFocus += Txt_filtreLit_LostFocus;//
            txt_filtreLit.GotFocus += Txt_filtreLit_GotFocus;*/
        }
        public void fonctionAffichePatient(string requeteAffichePatientFiltre)
        {
            // Déclaration des objets pour se connecter, requeter et afficher les resultats dans un DataGridView
            SqlConnection connection = new SqlConnection();
            SqlCommand command = new SqlCommand();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder();

            // On déclare la Chaine de connexion a la BDD
            connection.ConnectionString = @"Data Source=DESKTOP-9DDT0VE\SQLEXPRESS;Initial Catalog=medx;Integrated Security=True";
            // On prépare la requete avec la procédure stocké pour afficher tout les patiens ( sans filtre )
            string requeteAffichePatient = "EXEC S_PatientParFiltre ";
            requeteAffichePatient += requeteAffichePatientFiltre;
            Console.WriteLine(requeteAffichePatient);

            // On ouvre la connexion avec la BDD
            connection.Open();
            command = new SqlCommand(requeteAffichePatient, connection);
            dataAdapter = new SqlDataAdapter(command);
            // On remplis le Set de Donnée avec un Data Adapter
            dataAdapter.Fill(dataSet);
            // Source du DataGridView = Set de donnée que l'on a récuperé avant
            dgv_patientList.DataSource = dataSet.Tables[0];
            
        }

        private void btn_rechercherTri_Click(object sender, EventArgs e)
        {

            string parameters = string.Empty;
            if (radbtn_tousLit.Checked)
            {
                parameters = "@lit_Dispos = NULL ";
            }

            if (radbtn_disponibleLit.Checked)
            {
                parameters = "@lit_Dispos = 1 ";
            }

            if (radbtn_indisponibleLit.Checked)
            {
                parameters = "@lit_Dispos = 0 ";
            }

            if (txt_filtreLit.Text.Trim() != string.Empty)
            {
                parameters += ", @num_Lit = " + txt_filtreLit.Text.ToString()+" ";
            }
            fonctionAffichePatient(parameters);
        }


        #region Events

        private void dgv_patientListe_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            fen_modifPatient modifPatient = new fen_modifPatient();
            modifPatient.Show();
        }


     

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btn_ajoutPatient_Click(object sender, EventArgs e)
        {
            fen_modifPatient modifPatient = new fen_modifPatient();
            modifPatient.Show();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_filtreLit_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void gp_gestionServices_Enter(object sender, EventArgs e)
        {

        }

        private void txt_filtreLit_KeyPress(object sender, KeyPressEventArgs e)
        {
            // On n'autorise que les caractères numériques
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        #endregion

        #region placeHolders
        private void Txt_filtreLit_GotFocus(object sender, EventArgs e)
        {
            if (txt_filtreLit.Text == "Lit")
            {
                txt_filtreLit.ForeColor = Color.Black;
                txt_filtreLit.Text = "";
            }
        }

        private void Txt_filtreLit_LostFocus(object sender, EventArgs e)
        {
            if (txt_filtreLit.Text == "")
            {
                txt_filtreLit.ForeColor = Color.Gray;
                txt_filtreLit.Text = "Lit";
            }
        }





        #endregion

        private void btn_gererPathologies_Click(object sender, EventArgs e)
        {
            fen_gererPathologie gererPathologie = new fen_gererPathologie();
            gererPathologie.Show();
        }

        private void btn_gererEquipement_Click(object sender, EventArgs e)
        {
            fen_gererEquipement gererEquipement = new fen_gererEquipement();
            gererEquipement.Show();
        }

        private void btn_gererLits_Click(object sender, EventArgs e)
        {
            fen_gererLit gererLit = new fen_gererLit();
            gererLit.Show();
        }

        private void btn_gererChambre_Click(object sender, EventArgs e)
        {
            fen_gererChambre gererChambre = new fen_gererChambre();
            gererChambre.Show();
        }

        private void btn_gererSejours_Click(object sender, EventArgs e)
        {
            fen_gererSejour gererSejour = new fen_gererSejour();
            gererSejour.Show();
        }
    }
}

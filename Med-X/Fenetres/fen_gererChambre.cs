﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Med_X.Fenetres
{
    public partial class fen_gererChambre : Form
    {
        public fen_gererChambre()
        {
            InitializeComponent();
        }

        private void dgv_gererChambre_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            fen_modifChambre modifChambre = new fen_modifChambre();
            modifChambre.Show();
        }

        private void btn_ajoutChambre_Click(object sender, EventArgs e)
        {
            fen_modifChambre modifChambre = new fen_modifChambre();
            modifChambre.Show();
        }
    }
}

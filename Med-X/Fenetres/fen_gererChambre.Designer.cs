﻿
namespace Med_X.Fenetres
{
    partial class fen_gererChambre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_gererChambre = new System.Windows.Forms.DataGridView();
            this.btn_ajoutChambre = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_gererChambre)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_gererChambre
            // 
            this.dgv_gererChambre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_gererChambre.Location = new System.Drawing.Point(263, 205);
            this.dgv_gererChambre.Name = "dgv_gererChambre";
            this.dgv_gererChambre.Size = new System.Drawing.Size(240, 150);
            this.dgv_gererChambre.TabIndex = 0;
            this.dgv_gererChambre.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_gererChambre_CellContentClick);
            // 
            // btn_ajoutChambre
            // 
            this.btn_ajoutChambre.Location = new System.Drawing.Point(566, 295);
            this.btn_ajoutChambre.Name = "btn_ajoutChambre";
            this.btn_ajoutChambre.Size = new System.Drawing.Size(155, 23);
            this.btn_ajoutChambre.TabIndex = 1;
            this.btn_ajoutChambre.Text = "ajout Chambre";
            this.btn_ajoutChambre.UseVisualStyleBackColor = true;
            this.btn_ajoutChambre.Click += new System.EventHandler(this.btn_ajoutChambre_Click);
            // 
            // fen_gererChambre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_ajoutChambre);
            this.Controls.Add(this.dgv_gererChambre);
            this.Name = "fen_gererChambre";
            this.Text = "Gerer Chambre";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_gererChambre)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_gererChambre;
        private System.Windows.Forms.Button btn_ajoutChambre;
    }
}
﻿
namespace Med_X
{
    partial class fen_accueil
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fen_accueil));
            this.dgv_patientList = new System.Windows.Forms.DataGridView();
            this.medxDataSet = new Med_X.medxDataSet();
            this.tabPatientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tab_PatientTableAdapter = new Med_X.medxDataSetTableAdapters.tab_PatientTableAdapter();
            this.medxDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.medxDataSet1 = new Med_X.medxDataSet1();
            this.tabChambreBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tab_ChambreTableAdapter = new Med_X.medxDataSet1TableAdapters.tab_ChambreTableAdapter();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.gp_gestionPatients = new System.Windows.Forms.GroupBox();
            this.btn_ajoutPatient = new System.Windows.Forms.Button();
            this.gp_gestionServices = new System.Windows.Forms.GroupBox();
            this.btn_gererSejours = new System.Windows.Forms.Button();
            this.btn_gererChambre = new System.Windows.Forms.Button();
            this.btn_gererEquipement = new System.Windows.Forms.Button();
            this.btn_gererLits = new System.Windows.Forms.Button();
            this.btn_gererPathologies = new System.Windows.Forms.Button();
            this.radbtn_tousLit = new System.Windows.Forms.RadioButton();
            this.radbtn_indisponibleLit = new System.Windows.Forms.RadioButton();
            this.radbtn_disponibleLit = new System.Windows.Forms.RadioButton();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.pnl_filtreLit = new System.Windows.Forms.Panel();
            this.txt_filtreLit = new System.Windows.Forms.TextBox();
            this.txt_filtreChambre = new System.Windows.Forms.TextBox();
            this.txt_filtreNumDossier = new System.Windows.Forms.TextBox();
            this.txt_filtrePatient = new System.Windows.Forms.TextBox();
            this.pnl_filtres = new System.Windows.Forms.Panel();
            this.btn_rechercherTri = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_patientList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.medxDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPatientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.medxDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.medxDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabChambreBindingSource)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.gp_gestionPatients.SuspendLayout();
            this.gp_gestionServices.SuspendLayout();
            this.pnl_filtreLit.SuspendLayout();
            this.pnl_filtres.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_patientList
            // 
            this.dgv_patientList.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_patientList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_patientList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_patientList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.PowderBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_patientList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_patientList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgv_patientList.GridColor = System.Drawing.Color.Black;
            this.dgv_patientList.Location = new System.Drawing.Point(0, 224);
            this.dgv_patientList.Name = "dgv_patientList";
            this.dgv_patientList.ReadOnly = true;
            this.dgv_patientList.Size = new System.Drawing.Size(1141, 304);
            this.dgv_patientList.TabIndex = 0;
            // 
            // medxDataSet
            // 
            this.medxDataSet.DataSetName = "medxDataSet";
            this.medxDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPatientBindingSource
            // 
            this.tabPatientBindingSource.DataMember = "tab_Patient";
            this.tabPatientBindingSource.DataSource = this.medxDataSet;
            // 
            // tab_PatientTableAdapter
            // 
            this.tab_PatientTableAdapter.ClearBeforeFill = true;
            // 
            // medxDataSetBindingSource
            // 
            this.medxDataSetBindingSource.DataSource = this.medxDataSet;
            this.medxDataSetBindingSource.Position = 0;
            // 
            // medxDataSet1
            // 
            this.medxDataSet1.DataSetName = "medxDataSet1";
            this.medxDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabChambreBindingSource
            // 
            this.tabChambreBindingSource.DataMember = "tab_Chambre";
            this.tabChambreBindingSource.DataSource = this.medxDataSet1;
            // 
            // tab_ChambreTableAdapter
            // 
            this.tab_ChambreTableAdapter.ClearBeforeFill = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.gp_gestionPatients);
            this.flowLayoutPanel1.Controls.Add(this.gp_gestionServices);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1141, 107);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // gp_gestionPatients
            // 
            this.gp_gestionPatients.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gp_gestionPatients.Controls.Add(this.btn_ajoutPatient);
            this.gp_gestionPatients.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gp_gestionPatients.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gp_gestionPatients.ForeColor = System.Drawing.Color.Black;
            this.gp_gestionPatients.Location = new System.Drawing.Point(3, 3);
            this.gp_gestionPatients.Name = "gp_gestionPatients";
            this.gp_gestionPatients.Size = new System.Drawing.Size(200, 98);
            this.gp_gestionPatients.TabIndex = 1;
            this.gp_gestionPatients.TabStop = false;
            this.gp_gestionPatients.Text = "Gestion des patients";
            // 
            // btn_ajoutPatient
            // 
            this.btn_ajoutPatient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(196)))), ((int)(((byte)(125)))));
            this.btn_ajoutPatient.Location = new System.Drawing.Point(43, 36);
            this.btn_ajoutPatient.Name = "btn_ajoutPatient";
            this.btn_ajoutPatient.Size = new System.Drawing.Size(109, 43);
            this.btn_ajoutPatient.TabIndex = 0;
            this.btn_ajoutPatient.Text = "Ajouter un patient";
            this.btn_ajoutPatient.UseVisualStyleBackColor = false;
            this.btn_ajoutPatient.Click += new System.EventHandler(this.btn_ajoutPatient_Click);
            // 
            // gp_gestionServices
            // 
            this.gp_gestionServices.AutoSize = true;
            this.gp_gestionServices.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gp_gestionServices.Controls.Add(this.btn_gererSejours);
            this.gp_gestionServices.Controls.Add(this.btn_gererChambre);
            this.gp_gestionServices.Controls.Add(this.btn_gererEquipement);
            this.gp_gestionServices.Controls.Add(this.btn_gererLits);
            this.gp_gestionServices.Controls.Add(this.btn_gererPathologies);
            this.gp_gestionServices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gp_gestionServices.Location = new System.Drawing.Point(209, 3);
            this.gp_gestionServices.Name = "gp_gestionServices";
            this.gp_gestionServices.Size = new System.Drawing.Size(858, 98);
            this.gp_gestionServices.TabIndex = 2;
            this.gp_gestionServices.TabStop = false;
            this.gp_gestionServices.Text = "Gestions des services";
            this.gp_gestionServices.Enter += new System.EventHandler(this.gp_gestionServices_Enter);
            // 
            // btn_gererSejours
            // 
            this.btn_gererSejours.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(196)))), ((int)(((byte)(125)))));
            this.btn_gererSejours.Location = new System.Drawing.Point(743, 36);
            this.btn_gererSejours.Name = "btn_gererSejours";
            this.btn_gererSejours.Size = new System.Drawing.Size(109, 43);
            this.btn_gererSejours.TabIndex = 5;
            this.btn_gererSejours.Text = "Gérer les séjours";
            this.btn_gererSejours.UseVisualStyleBackColor = false;
            this.btn_gererSejours.Click += new System.EventHandler(this.btn_gererSejours_Click);
            // 
            // btn_gererChambre
            // 
            this.btn_gererChambre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(196)))), ((int)(((byte)(125)))));
            this.btn_gererChambre.Location = new System.Drawing.Point(575, 36);
            this.btn_gererChambre.Name = "btn_gererChambre";
            this.btn_gererChambre.Size = new System.Drawing.Size(109, 43);
            this.btn_gererChambre.TabIndex = 4;
            this.btn_gererChambre.Text = "Gérer les chambres";
            this.btn_gererChambre.UseVisualStyleBackColor = false;
            this.btn_gererChambre.Click += new System.EventHandler(this.btn_gererChambre_Click);
            // 
            // btn_gererEquipement
            // 
            this.btn_gererEquipement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(196)))), ((int)(((byte)(125)))));
            this.btn_gererEquipement.Location = new System.Drawing.Point(225, 36);
            this.btn_gererEquipement.Name = "btn_gererEquipement";
            this.btn_gererEquipement.Size = new System.Drawing.Size(144, 43);
            this.btn_gererEquipement.TabIndex = 3;
            this.btn_gererEquipement.Text = "Gérer les équipements";
            this.btn_gererEquipement.UseVisualStyleBackColor = false;
            this.btn_gererEquipement.Click += new System.EventHandler(this.btn_gererEquipement_Click);
            // 
            // btn_gererLits
            // 
            this.btn_gererLits.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(196)))), ((int)(((byte)(125)))));
            this.btn_gererLits.Location = new System.Drawing.Point(402, 36);
            this.btn_gererLits.Name = "btn_gererLits";
            this.btn_gererLits.Size = new System.Drawing.Size(109, 43);
            this.btn_gererLits.TabIndex = 2;
            this.btn_gererLits.Text = "Gérer les lits";
            this.btn_gererLits.UseVisualStyleBackColor = false;
            this.btn_gererLits.Click += new System.EventHandler(this.btn_gererLits_Click);
            // 
            // btn_gererPathologies
            // 
            this.btn_gererPathologies.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(196)))), ((int)(((byte)(125)))));
            this.btn_gererPathologies.Location = new System.Drawing.Point(21, 36);
            this.btn_gererPathologies.Name = "btn_gererPathologies";
            this.btn_gererPathologies.Size = new System.Drawing.Size(150, 43);
            this.btn_gererPathologies.TabIndex = 1;
            this.btn_gererPathologies.Text = "Gérer les pathologies";
            this.btn_gererPathologies.UseVisualStyleBackColor = false;
            this.btn_gererPathologies.Click += new System.EventHandler(this.btn_gererPathologies_Click);
            // 
            // radbtn_tousLit
            // 
            this.radbtn_tousLit.Appearance = System.Windows.Forms.Appearance.Button;
            this.radbtn_tousLit.AutoSize = true;
            this.radbtn_tousLit.Checked = true;
            this.radbtn_tousLit.Dock = System.Windows.Forms.DockStyle.Left;
            this.radbtn_tousLit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.radbtn_tousLit.FlatAppearance.BorderSize = 2;
            this.radbtn_tousLit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radbtn_tousLit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radbtn_tousLit.Location = new System.Drawing.Point(0, 0);
            this.radbtn_tousLit.Name = "radbtn_tousLit";
            this.radbtn_tousLit.Size = new System.Drawing.Size(53, 38);
            this.radbtn_tousLit.TabIndex = 3;
            this.radbtn_tousLit.TabStop = true;
            this.radbtn_tousLit.Text = "Tous";
            this.radbtn_tousLit.UseVisualStyleBackColor = true;
            this.radbtn_tousLit.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radbtn_indisponibleLit
            // 
            this.radbtn_indisponibleLit.Appearance = System.Windows.Forms.Appearance.Button;
            this.radbtn_indisponibleLit.AutoSize = true;
            this.radbtn_indisponibleLit.Dock = System.Windows.Forms.DockStyle.Right;
            this.radbtn_indisponibleLit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.radbtn_indisponibleLit.FlatAppearance.BorderSize = 2;
            this.radbtn_indisponibleLit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radbtn_indisponibleLit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radbtn_indisponibleLit.Location = new System.Drawing.Point(143, 0);
            this.radbtn_indisponibleLit.Name = "radbtn_indisponibleLit";
            this.radbtn_indisponibleLit.Size = new System.Drawing.Size(95, 38);
            this.radbtn_indisponibleLit.TabIndex = 4;
            this.radbtn_indisponibleLit.Text = "Indisponible";
            this.radbtn_indisponibleLit.UseVisualStyleBackColor = true;
            // 
            // radbtn_disponibleLit
            // 
            this.radbtn_disponibleLit.Appearance = System.Windows.Forms.Appearance.Button;
            this.radbtn_disponibleLit.AutoSize = true;
            this.radbtn_disponibleLit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radbtn_disponibleLit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.radbtn_disponibleLit.FlatAppearance.BorderSize = 2;
            this.radbtn_disponibleLit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radbtn_disponibleLit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radbtn_disponibleLit.Location = new System.Drawing.Point(53, 0);
            this.radbtn_disponibleLit.Name = "radbtn_disponibleLit";
            this.radbtn_disponibleLit.Size = new System.Drawing.Size(90, 38);
            this.radbtn_disponibleLit.TabIndex = 5;
            this.radbtn_disponibleLit.Text = "Disponible";
            this.radbtn_disponibleLit.UseVisualStyleBackColor = true;
            // 
            // pnl_filtreLit
            // 
            this.pnl_filtreLit.Controls.Add(this.radbtn_disponibleLit);
            this.pnl_filtreLit.Controls.Add(this.radbtn_tousLit);
            this.pnl_filtreLit.Controls.Add(this.radbtn_indisponibleLit);
            this.pnl_filtreLit.Location = new System.Drawing.Point(12, 129);
            this.pnl_filtreLit.Name = "pnl_filtreLit";
            this.pnl_filtreLit.Size = new System.Drawing.Size(238, 38);
            this.pnl_filtreLit.TabIndex = 6;
            // 
            // txt_filtreLit
            // 
            this.txt_filtreLit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_filtreLit.Location = new System.Drawing.Point(9, 11);
            this.txt_filtreLit.Name = "txt_filtreLit";
            this.txt_filtreLit.Size = new System.Drawing.Size(38, 24);
            this.txt_filtreLit.TabIndex = 7;
            this.txt_filtreLit.TextChanged += new System.EventHandler(this.txt_filtreLit_TextChanged);
            this.txt_filtreLit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_filtreLit_KeyPress);
            // 
            // txt_filtreChambre
            // 
            this.txt_filtreChambre.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_filtreChambre.Location = new System.Drawing.Point(277, 11);
            this.txt_filtreChambre.Name = "txt_filtreChambre";
            this.txt_filtreChambre.Size = new System.Drawing.Size(100, 24);
            this.txt_filtreChambre.TabIndex = 8;
            this.txt_filtreChambre.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txt_filtreNumDossier
            // 
            this.txt_filtreNumDossier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_filtreNumDossier.Location = new System.Drawing.Point(171, 11);
            this.txt_filtreNumDossier.Name = "txt_filtreNumDossier";
            this.txt_filtreNumDossier.Size = new System.Drawing.Size(100, 24);
            this.txt_filtreNumDossier.TabIndex = 9;
            // 
            // txt_filtrePatient
            // 
            this.txt_filtrePatient.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_filtrePatient.Location = new System.Drawing.Point(56, 11);
            this.txt_filtrePatient.Name = "txt_filtrePatient";
            this.txt_filtrePatient.Size = new System.Drawing.Size(109, 24);
            this.txt_filtrePatient.TabIndex = 10;
            // 
            // pnl_filtres
            // 
            this.pnl_filtres.Controls.Add(this.btn_rechercherTri);
            this.pnl_filtres.Controls.Add(this.txt_filtreLit);
            this.pnl_filtres.Controls.Add(this.txt_filtrePatient);
            this.pnl_filtres.Controls.Add(this.txt_filtreChambre);
            this.pnl_filtres.Controls.Add(this.txt_filtreNumDossier);
            this.pnl_filtres.Location = new System.Drawing.Point(3, 173);
            this.pnl_filtres.Name = "pnl_filtres";
            this.pnl_filtres.Size = new System.Drawing.Size(1136, 45);
            this.pnl_filtres.TabIndex = 11;
            // 
            // btn_rechercherTri
            // 
            this.btn_rechercherTri.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(196)))), ((int)(((byte)(125)))));
            this.btn_rechercherTri.Location = new System.Drawing.Point(983, 2);
            this.btn_rechercherTri.Name = "btn_rechercherTri";
            this.btn_rechercherTri.Size = new System.Drawing.Size(150, 43);
            this.btn_rechercherTri.TabIndex = 6;
            this.btn_rechercherTri.Text = "Rechercher";
            this.btn_rechercherTri.UseVisualStyleBackColor = false;
            this.btn_rechercherTri.Click += new System.EventHandler(this.btn_rechercherTri_Click);
            // 
            // fen_accueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SeaGreen;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1141, 528);
            this.Controls.Add(this.pnl_filtres);
            this.Controls.Add(this.pnl_filtreLit);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.dgv_patientList);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fen_accueil";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Fen_accueil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_patientList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.medxDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPatientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.medxDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.medxDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabChambreBindingSource)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.gp_gestionPatients.ResumeLayout(false);
            this.gp_gestionServices.ResumeLayout(false);
            this.pnl_filtreLit.ResumeLayout(false);
            this.pnl_filtreLit.PerformLayout();
            this.pnl_filtres.ResumeLayout(false);
            this.pnl_filtres.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_patientList;
        private medxDataSet medxDataSet;
        private System.Windows.Forms.BindingSource tabPatientBindingSource;
        private medxDataSetTableAdapters.tab_PatientTableAdapter tab_PatientTableAdapter;
        private System.Windows.Forms.BindingSource medxDataSetBindingSource;
        private medxDataSet1 medxDataSet1;
        private System.Windows.Forms.BindingSource tabChambreBindingSource;
        private medxDataSet1TableAdapters.tab_ChambreTableAdapter tab_ChambreTableAdapter;
        private System.Windows.Forms.Button btn_ajoutPatient;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox gp_gestionServices;
        private System.Windows.Forms.Button btn_gererSejours;
        private System.Windows.Forms.Button btn_gererChambre;
        private System.Windows.Forms.Button btn_gererEquipement;
        private System.Windows.Forms.Button btn_gererLits;
        private System.Windows.Forms.Button btn_gererPathologies;
        private System.Windows.Forms.RadioButton radbtn_tousLit;
        private System.Windows.Forms.RadioButton radbtn_indisponibleLit;
        private System.Windows.Forms.RadioButton radbtn_disponibleLit;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox gp_gestionPatients;
        private System.Windows.Forms.Panel pnl_filtreLit;
        private System.Windows.Forms.TextBox txt_filtreLit;
        private System.Windows.Forms.TextBox txt_filtreChambre;
        private System.Windows.Forms.TextBox txt_filtreNumDossier;
        private System.Windows.Forms.TextBox txt_filtrePatient;
        private System.Windows.Forms.Panel pnl_filtres;
        private System.Windows.Forms.Button btn_rechercherTri;
    }
}


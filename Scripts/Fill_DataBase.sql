USE medx
GO

INSERT INTO tab_Sejour( 
	s_type_Sejour
	,s_nombre_Jours
)
VALUES(
	'Court'
	,1
	)
	,(
	'Moyen'
	,3
	)
	,(
	'Long'
	,5
	)

INSERT INTO tab_Equipement( 
	e_nom_Equipement
)
VALUES	('Respirateur')
		,('Defibrilateur')
		,('Elevateur')

INSERT INTO tab_Chambre( 
	c_num_Chambre
)
VALUES	(100)
		,(101)
		,(102)

INSERT INTO tab_Pathologie( 
	pa_nom_Pathologie
	,pa_id_Equipement
)
VALUES	(
	'Arr�t cardiaque'
	,2
	)
	,(
	'COVID'
	,1
	)
	,(
	'Tetrapl�gique'
	,3
	)


INSERT INTO tab_Patient(
	p_nom_Patient
	,p_prenom_Patient
	,p_num_Dossier
	,p_id_Pathologie
	,p_id_Sejour
)
VALUES	(
		'Reporter'
		,'Albert'
		,'M1001'
		,2
		,3
		)
		,(
		'Tessier'
		,'Jorisse'
		,'M1002'
		,3
		,3
		)

INSERT INTO tab_Lit( 
	l_num_Lit
	,l_id_Equipement
	,l_id_Chambre
	,l_id_Patient
)
VALUES	(
	1
	,1
	,1
	,1
	)
	,(
	2
	,2
	,1
	,NULL
	)
	,(
	3
	,1
	,1
	,2
	)

CREATE DATABASE medx

GO
USE medx 
GO

CREATE TABLE tab_Patient (
  p_id_Patient INT PRIMARY KEY IDENTITY(1, 1)
  ,p_nom_Patient VARCHAR(255)
  ,p_prenom_Patient VARCHAR(255)
  ,p_num_Dossier VARCHAR(255)
  ,p_id_Pathologie INT
  ,p_id_Sejour INT
  ,p_date_Creation DATETIME DEFAULT GETDATE() 
  ,p_date_Modification DATETIME
  ,p_date_Suppression DATETIME
  ,p_ligne_Suppression BIT DEFAULT NULL
)

GO

CREATE TABLE tab_Pathologie (
  pa_id_Pathologie INT PRIMARY KEY IDENTITY(1, 1)
  ,pa_nom_Pathologie VARCHAR(255)
  ,pa_id_Equipement INT
  ,pa_date_Creation DATETIME DEFAULT GETDATE() 
  ,pa_date_Modification DATETIME
  ,pa_date_Suppression DATETIME
  ,pa_ligne_Suppression BIT DEFAULT NULL
)

GO

CREATE TABLE tab_Lit (
  l_id_Lit INT PRIMARY KEY IDENTITY(1, 1)
  ,l_num_Lit INT
  ,l_id_Equipement INT
  ,l_id_Chambre INT
  ,l_id_Patient INT
  ,l_date_Creation DATETIME DEFAULT GETDATE() 
  ,l_date_Modification DATETIME
  ,l_date_Suppression DATETIME
  ,l_ligne_Suppression BIT DEFAULT NULL
)

GO

CREATE TABLE tab_Chambre (
  c_id_Chambre INT PRIMARY KEY IDENTITY(1, 1)
  ,c_num_Chambre INT
  ,c_date_Creation DATETIME DEFAULT GETDATE() 
  ,c_date_Modification DATETIME
  ,c_date_Suppression DATETIME
  ,c_ligne_Suppression BIT DEFAULT NULL
)

GO

CREATE TABLE tab_Sejour (
  s_id_Sejour INT PRIMARY KEY IDENTITY(1, 1)
  ,s_type_Sejour VARCHAR(255)
  ,s_nombre_Jours INT
  ,s_date_Creation DATETIME DEFAULT GETDATE() 
  ,s_date_Modification DATETIME
  ,s_date_Suppression DATETIME
  ,s_ligne_Suppression BIT DEFAULT NULL
)

GO

CREATE TABLE tab_Equipement (
  e_id_Equipement INT PRIMARY KEY IDENTITY(1, 1)
  ,e_nom_Equipement VARCHAR(255)
  ,e_date_Creation DATETIME DEFAULT GETDATE() 
  ,e_date_Modification DATETIME
  ,e_date_Suppression DATETIME
  ,e_ligne_Suppression BIT DEFAULT NULL
)

GO

USE medx 
GO

DROP PROCEDURE S_PatientParFiltre
GO
CREATE PROCEDURE S_PatientParFiltre (
	@num_Lit INT = NULL
	,@rech_Patient VARCHAR(30) = NULL
	,@num_Dossier VARCHAR(10) = NULL
	,@num_Chambre INT = NULL
	,@id_Pathologie INT = NULL
	,@id_Equipement INT = NULL
	,@id_Duree_Sejour INT = NULL
	,@lit_Dispos BIT = NULL
)
AS
BEGIN
	
	SELECT
		L.l_num_Lit as 'NuméroDeLit'
		,P.p_prenom_Patient + ' ' + P.p_nom_Patient as 'NomDuPatient'
		,P.p_num_Dossier as 'NuméroDeDossier'
		,C.c_num_Chambre as 'NuméroDeChambre'
		,PA.pa_nom_Pathologie as 'NomDeLaPathologie'
		,E.e_nom_Equipement as 'NomDeLequipement'
		,S.s_type_Sejour as 'DuréeDeSéjour'
		,ISNUMERIC(L.l_id_Patient) as 'LitDispo'
	INTO #LISTE_LIT
	FROM tab_Lit L
	LEFT JOIN tab_Patient P ON L.l_id_Patient = P.p_id_Patient
	LEFT JOIN tab_Chambre C ON L.l_id_Chambre = C.c_id_Chambre
	LEFT JOIN tab_Pathologie PA ON P.p_id_Pathologie = PA.pa_id_Pathologie
	LEFT JOIN tab_Equipement E ON PA.pa_id_Equipement = E.e_id_Equipement
	LEFT JOIN tab_Sejour S ON P.p_id_Sejour = S.s_id_Sejour
	WHERE @num_Lit IS NULL OR (L.l_num_Lit = @num_Lit)
	AND @rech_Patient IS NULL OR ( (P.p_prenom_Patient LIKE '%'+@rech_Patient+'%') OR (P.p_nom_Patient LIKE '%'+@rech_Patient+'%') )
	AND @num_Dossier IS NULL OR (P.p_num_Dossier = @num_Dossier)
	AND @num_Chambre IS NULL OR (C.c_num_Chambre = @num_Chambre)
	AND @id_Pathologie IS NULL OR (PA.pa_id_Pathologie = @id_Pathologie)
	AND @id_Equipement IS NULL OR (E.e_id_Equipement = @id_Equipement)
	AND @id_Duree_Sejour IS NULL OR (S.s_id_Sejour = @id_Duree_Sejour)

	SELECT
		NuméroDeLit
		,NomDuPatient
		,NuméroDeDossier
		,NuméroDeChambre
		,NomDeLaPathologie
		,NomDeLequipement
		,DuréeDeSéjour
	FROM #LISTE_LIT
	WHERE (
		@lit_Dispos IS NULL 
		OR LitDispo = ( 
			CASE
				WHEN @lit_Dispos = 1 THEN CAST(0 AS BIT)
				WHEN @lit_Dispos = 0 THEN CAST(1 AS BIT)
			END
		)
	)

END